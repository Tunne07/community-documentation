---
title: French - Feedback Survey Template
keywords: modèle d'email, commentaires des clients, politique de traitement des incidents, suivi
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Modèle d'un email pour demander l'avis des clients à propos leurs demandes à la fin de chaque ticket"
sidebar: mydoc_sidebar
permalink: 351-fr_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: fr
---


# French - Feedback Survey Template
## Modèle d'un email pour demander l'avis des clients à propos leurs demandes à la fin de chaque ticket

### Body

Bonjour {{ beneficiary name }},

Merci de contacter l'équipe Helpline, gérée par l’organisation internationale des droits de l’homme Access Now - https://accessnow.org.

Ce message a pour but de vous informer de la clôture de votre incident, intitulé "{{ email subject }}".

Votre avis est important pour nous. Si vous souhaitez nous faire contribuer avec vos commentaires sur votre expérience avec l'équipe Helpline Access Now, veuillez vous s'il vous plaît remplir le questionnaire suivant:

https://form.accessnow.org/index.php/139723?lang=fr&139723X20X841={{ ticket id }}

Le numero de votre incident est: {{ ticket id }}

Si vous avez d'autres questions ou préoccupations, veuillez nous informer et nous serons heureux de fournir notre assistance.

Merci,

{{ incident handler name }}
