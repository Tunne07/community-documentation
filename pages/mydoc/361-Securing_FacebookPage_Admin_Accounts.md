---
title: Securing Facebook Pages Admin Accounts
keywords: authentication, verification, 2-factor authentication, two-factor authentication, 2-step verification, 2FA, multi-factor authentication, authentication security, securing accounts, social media, social networking, facebook, facebook pages, identity management, separating accounts
last_updated: September 24, 2019
tags: [account_security, articles]
summary: "A client is concerned that the accounts they use to manage their project's Facebook Page might be identified and targeted by harassers, or that the account might be or might have been compromised, so they need basic recommendations on how to secure the administration accounts."
sidebar: mydoc_sidebar
permalink: 361-Securing_FacebookPage_Admin_Accounts.html
folder: mydoc
conf: Public
ref: Securing_FacebookPage_Admin_Accounts
lang: en
---


# Securing Facebook Pages Admin Accounts
## Basic security advice for Facebook Pages administration accounts

### Problem

Facebook has become crucial for many human rights activists to share their ideas, communicate with partners, and broadcast news. Their work and popularity can make them targets for harassment, hacking, and account compromise. Facebook Pages are normally used by organizations and independent media to spread their messages. The administration accounts of these pages become targets of attack and are exposed to harassment, censorship, account compromise, and doxxing. Securing these accounts requires a more in-depth understanding of the platform and possible vectors of attack.

This article focuses primarily on how to secure accounts used for the administration of Facebook Pages.

A case may start because:

- A client has received an unsolicited recovery link
- There is evidence that the account has been used by unauthorized persons without the owner's knowledge
- A client is concerned by attempts at accessing their account from devices they don't recognize
- A client is starting a campaign on their Facebook page and would like to prevent attacks against their account
- A client reports that their page is being targeted by attacks and harassment
- A client is concerned that their involvement with the page may expose their information or put them at risk
- A client is concerned for the privacy and security of their Facebook page

In all such cases, we should work with the client to develop a secure approach to the management of their Facebook Page, and make sure their account(s) cannot be accessed by unauthorized parties or connected to their personal life.


* * *


### Solution

Add layers of security for greater protection:

#### Use a new account only for the admin role

Create an account that is used solely for the purpose of managing the Facebook Page. This account should not have any personal information, pictures or posts, and should not be linked to or communicate with the owner's personal account. This account should also have as few contacts as possible, best if not overlapping with the personal account's contacts. The user may decide whether they want to use their real name or not. 

*NOTE: Using a fake name or pseudonym puts them at risk of the account being reported and shutdown, but these accounts for admin roles should be considered disposable.*

#### Have backup admin accounts in case one gets removed

Having only one admin account can be risky - if the account is compromised or removed, the user will lose access to the page.

In groups, each person who manages the page should have their own admin account, which can also be used to add a new account to the admin list when an account is removed.

If the page is managed by just one person, suggest them to create at least a backup account that is added to the admin list.

#### Configure the privacy settings to hide everything from everyone

Accounts used for admin roles will not be used for regular posting, so they can have the privacy settings configured to hide all information.

[Facebook Privacy Checkup](https://www.facebook.com/help/443357099140264)

#### Secure the accounts! 

This is perhaps the most important step, as an account compromise could be very harmful (for example, the attacker could remove the page, delete other admins, use the page for hacking into the other admin accounts through phishing, gather more sensitive information like email addresses and phone numbers, etc). Make sure that the admin accounts are using 2-factor authentication and strong passwords, and that good practices are followed. This should be applied to the Facebook account and also to any email account connected to the page.

Follow [Article #95: FAQ - Securing Online Accounts](95-FAQ_Securing_Online_Accounts.html) for each account.


* * *


### Comments

If the account is known to be compromised, please see [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html).

More tips on managing different accounts and identities securely can be found in [this guide](https://gendersec.tacticaltech.org/wiki/index.php/Step_1).


* * *


### Related Articles

- [Article #95: FAQ - Securing Online Accounts](95-FAQ_Securing_Online_Accounts.html)
- [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html)