---
title: Host-Based Live Forensics on Windows
keywords: forensics, built-in tools, command line, Windows
last_updated: February 10, 2020
tags: [forensics, articles]
summary: "The Helpline needs to perform live forensics on a Windows machine using built-in tools."
sidebar: mydoc_sidebar
permalink: 367-Live_Forensics_for_Windows.html
folder: mydoc
conf: Public
lang: en
---


# Host-Based Live Forensics on Windows
## How to perform live forensics on Windows machines using the available built-in tools

### Problem

In some instances, a Windows machine may need to be isolated from the shared network or internet access point it is usually connected to due to its compromised state. This article applies to a similar scenario, where we have limited resources to conduct forensic analysis and have to rely on the built-in tools available in the device.


* * *


### Solution

Below is the general methodology we can follow when conducting live forensics on a Windows machine. Please switch to the **Administrator** mode to ensure their full functionality.

#### 1. Unusual Network Entries

There are protocols in a Windows machine that can be exploited/abused by attackers both on the client and server side (e.g. Server Message Block - SMB). The following commands will list the sessions and connection that we can use for our initial investigation:

    c:\> net session

Displays a list of domains, computers, or resources that are being shared by the specified computer.

    c:\> net use

Shows detailed information on currently mapped drives and devices.

    c:\> netstat -na

OR  

    c:\> netstat -naob

Displays listening UDP and TCP ports and remote IPs. Adding -o shows corresponding programs' PIDS and adding -b shows corresponding .exe files.


#### 2. Unusual Processes and Services

Check unknown processes run by users with high privileges (i.e. SYSTEM and Administrator) or users in Administrator group using:

`c:\> taskmgr.exe` (GUI)

OR 

    c:\> tasklist -v

OR 

    c:\> wmic process list full

*Note: the output of the latter command will include the full command used for starting the service and its verbose description. It can be a quite long list.*

    c:\> wmic process where processid=[PID]

Will look into a specific program using its PID.

    c:\> wmic process where processid=[PID]  get commandline, parentprocessid

This command can be launched to get the associated command line.

*Note: A suspicious command line could be base64.*

Tools we can use to check the running services are:

`c:\> services.msc`(GUI)

OR

    c:\> sc query


#### 3. Unusual Scheduled Tasks

Many attacks use Windows Task Scheduler to hide and set up various tasks to further infiltrate the system. To investigate/audit scheduled task, you can use one of the following commands: 

`c:\> taskschd.msc` (GUI)

OR

    c:\> schtasks | more

    c:\> schtasks /query /TN TaskName /v /FO list

The latter command can be used to list a specific task.


#### 4. Unusual Startup Programs

Look for unusual programs referred to in registry keys associated with system startup by using the following commands:

`c:\> regedit` (GUI) 

OR 

    c:\>  **reg query [RegKey]

RegKey:

- HKLM\Software\Microsoft\Windows\CurrentVersion\Run
- HKLM\Software\Microsoft\Windows\CurrentVersion\Runonce
- HKLM\Software\Microsoft\Windows\CurrentVersion\RunonceEx
- HKCU\Software\Microsoft\Windows\CurrentVersion\Run
- HKCU\Software\Microsoft\Windows\CurrentVersion\Runonce
- HKCU\Software\Microsoft\Windows\CurrentVersion\RunonceEx

Another way to check startup programs is with `wmic` via Command Prompt:

    c:\> wmic startup list full


#### 5. Unusual Accounts

Unrecognizable users should always attract attention. An unrecognizable user in the administrators group is a particularly serious issue, as this user has all privileges to work on the machine. Also the creation of this user may be the result of a successful privilege escalation attack that the system is vulnerable to.

Look for unexpected users in the Administrators group by entering the following command: 

`c:\> lusrmgr.msc` (GUI) 

OR

    c:\> net user

OR

    net localgroup administrators


#### 6. Unusual or Gapped Log Entries

Launch this command:

`c:\> eventvwr.msc` (GUI) 

and look for the following events:

- **4625**  **Logon – Failed Logon** - This is the event code for failed logon attempts. If you find many of them concentrated in a very short period of time, this could mean that there was a (local) brute-force attack against this system. Check the details to figure out what account was conducting these logon attempts.

- **4624**  **Logon – Successful Logon** - Check if there is any 4624 event, which would show that the logon attempts eventually succeeded. If this has happened, you are probably facing a serious issue that should be dealt with urgently.

- **1102**  **Cleared Events** - This event code is an indicator of log cleaning. It could be read as a sign that an attacker was able to escalate their privilege to a system or administrator level and tried to hide what they did, clearing the logs. 

An advanced attacker could clear the logs as well as these event, for example using mimikatz.  What we are left with in these cases is to look for any gaps in the timestamp of logs.


* * *

#### More commands

- **| find “string”** - To search for a specific string in the output.

- **> File.txt** - To save the output into a text file.


* * *


### Comments

Please check [this presentation](https://drive.google.com/file/d/1bdyQlJLDWw5aq1EP_IhYTW2t-tOQh3tu/view?usp=sharing) to see typical outputs of most of the commands Listed in this article.

For more graphical tools (requires download and internet connection), check the  [Security Without Borders Guides](https://guides.securitywithoutborders.org/guide-to-quick-forensics/windows.html).


* * *


### Related Articles
