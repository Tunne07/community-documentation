---
title: WebRTC - recommendations and troubleshooting
keywords: VoIP, video chat, voice calls, video calls, WebRTC, Jitsi Meet, BigBlueButton, connection issues
last_updated: June 22, 2020
tags: [secure_communications, articles]
summary: "Jitsi and BigBlueButton are pretty user-friendly, but sometimes there are issues connecting to them. This article includes recommendations on clients and browsers and troubleshooting advice for connection issues."
sidebar: mydoc_sidebar
permalink: 402-WebRTC_recommendations_troubleshooting.html
folder: mydoc
conf: Public
lang: en
---


# WebRTC - recommendations and troubleshooting
## Jitsi Meet and BigBlueButton - recommendations on clients, browsers, and troubleshooting

### Problem

[Jitsi Meet](https://jitsi.org/) and [BigBlueButton](https://bigbluebutton.org/) are free and open source video conferencing tools that can be self-hosted and are pretty user-friendly. While most users have no problems using these services, some report issues with browser compatibility, as well as connection issues and problems with their microphone or camera.

When connecting to both BigBlueButton and Jitsi, Firefox seems to encounter the greatest number of problems, but some people have also reported issues with Chrome and Chrome-based browsers with certain add-ons and extensions.

The bottom line is that all video conferencing software relies on Web Real Time Communication in the browser (WebRTC), and many issues with Jitsi and BBB stem from browser issues with WebRTC.

#### What is WebRTC and why do we need it?

**WebRTC** (**Web Real-Time Communication**) is a free, open-source project that provides web browsers and mobile applications with real-time communication (RTC) via simple application programming interfaces (APIs). It allows audio and video communication to work inside web pages by allowing direct peer-to-peer communication, eliminating the need to install plugins or download native apps.

#### Is WebRTC secure?

Short answer: No. The use of WebRTC opens up a whole set of security concerns, most commonly, the revelation, or "leak", of one's IP address. More information [here](https://www.w3.org/wiki/Privacy/IPAddresses).

We have to be aware that to use Jitsi or BigBlueButton, which are WebRTC-based services, we have to give up our IP address for the clients (browsers) to send and receive constant streams for audio and video. The IP in question here is the public facing IP address that your router is assigned from your ISP.

**Note:** A workaround is to use a VPN if you want to hide your IP and location.

#### What browsers support WebRTC?

A good overview can be found [here](https://caniuse.com/#search=WebRTC).


* * *


### Solution

#### Recommended browsers for Jitsi Meet and BigBlueButton

**Jitsi** recommends Chrome or Chromium-based browsers, at least until the Mozilla team bring Firefox up to speed with supporting it ([see this thread for more information](https://community.jitsi.org/t/jitsi-meet-google-chrome-vs-firefox/17093/21 )). For best results, the Jitsi app should be used.

- Apps for mobile are [linked from Jitsi's website](https://jitsi.org/downloads/).
- On Windows, macOS, and Linux, you can use the Chrome-based [electron app](https://github.com/jitsi/jitsi-meet-electron), which includes an [end-to-end encryption feature](https://jitsi.org/blog/e2ee/) (still in testing stage).

**BigBlueButton** [recommends both Firefox and Chrome or Chrome-based browsers](http://docs.bigbluebutton.org/support/faq.html#what-are-the-minimum-requirements-for-the-bigbluebutton-client).


#### Troubleshooting common issues

##### Why can't I use my microphone or camera with Firefox?

While some users have had absolutely no problems connecting with Firefox, many users have reported issues connecting to both Jitsi and BBB.

There could be 2 causes:

1. **Firefox has an add-on/extension that blocks WebRTC.**

    Add-ons like [uBlock origin](https://github.com/gorhill/uBlock/) block WebRTC connections by default to prevent the inadvertent leaking of IP addresses on websites that have widgets or embedded WebRTC elements.
To use a video conferencing service like Jitsi or BigBlueButton you have to explicitly whitelist the URL of the service in the settings of your add-on, or disable the add-on temporarily for the session. Please note that this will potentially expose your IP address.

    ***Tip:** To whitelist a URL on uBlock, uncheck the box `ublock > settings >  Prevent WebRTC from leaking your local IP address`.

2. **Firefox has some settings that are blocking WebRTC.**

    To fix this:

        **Note:** Please proceed with caution with these steps!

    - Open a new tab in Firefox
    - Type `about:config` in the location bar (where you normally type in the URL of a webpage)
    - You will see a warning like the one below

        ![Firefox warning](../../images/firefox_warning.png)

    - Search for `media.peerconnection.enabled`
    - Make sure the value of the `media.peerconnection.enabled` setting is "false" by clicking the toggle button on the right

        ![media.peerconnection.enabled](../../images/media-peerconnection-enabled.png)

    - Close the settings tab.

##### Why can't I use my microphone or camera with Chrome or Chromium / Brave?

Chrome- and Chromium-based browsers like Brave allow WebRTC by default. If you are facing problems, it could well be you have an add-on or extension installed that is blocking WebRTC. If you have uBlock Origin installed, please follow the steps as described above in the [firefox section](#firefox-has-an-add-on/extension-that-blocks-webrtc).


* * *


### Comments


* * *


### Related Articles
