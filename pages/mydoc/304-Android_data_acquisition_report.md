---
title: Mobile Data Acquisition Report Guidelines
keywords: mobile, forensics, malware, live imaging, report
last_updated: January 30, 2019
tags: [forensics, articles]
summary: "The Helpline has acquired data from an Android device that could be infected or can be used a digital evidence, and needs to issue a report on the forensic acquisition."
sidebar: mydoc_sidebar
permalink: 304-Mobile_data_acquisition_report_guidelines.html
folder: mydoc
conf: Public
lang: en
---


# Mobile Data Acquisition Report Guidelines
## Guidelines for producing a report of a forensic data acquisition on a mobile device

### Problem

All the steps detailed in [Article #305: Android Devices Data Acquisition Procedure](305-Data_acquisition_Android_Debug_Bridge.html) (SD Card and SIM card byte-copy, data acquisition with Android Debug Bridge) should be detailed in this report, along with the results of the previous research and data gathered on the device.


* * *


### Solution

Create a report with the following sections:

- Title page: Case name (Access Now Ticket number), date, investigator's name, and contact information
- Table of Contents
- Executive Summary
- Aim of the investigation and objectives
- Device research and data: general information, condition of the device, hardware structure, file system, etc.
- Selected acquisition tool and justification of the selection (include software and hardware used, version numbers, etc.)
- Procedures followed: SD Card and SIM card byte-copy, data acquisition with Android Debug Bridge
- Image information summary: image name, hashes, name of the encrypted container, etc.
- Timeline: concise timeline of important events
- Conclusion
- Signature
- Investigator's curriculum vitae, chain of custody documentation, supporting document linked from the body of the report, etc.


* * *


### Comments

Find an official format for the report [here]().


#### Notes

- [Report Writing Guidelines](https://www.forensicmag.com/article/2012/05/report-writing-guidelines)
- [Preparing an Expert Report](https://www.forensicmag.com/article/2011/07/preparing-expert-report)
- [NIST chain of custody sample](https://www.nist.gov/sites/default/files/documents/2017/04/28/Sample-Chain-of-Custody-Form.docx) [.docx]


* * *


### Related Articles

- [Article #305: Android Devices Data Acquisition Procedure](305-Data_acquisition_Android_Debug_Bridge.html)