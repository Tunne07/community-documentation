---
title: DDoS Attack Identification
keywords: DDoS, Website, CDN 
last_updated:  May 9, 2020
tags: [ddos_attack, articles, faq]
summary: "A website is unreachable and the client needs to understand why and what to do"
sidebar: mydoc_sidebar
permalink: DDoS_Attack_Identification.html 
folder: mydoc
conf: Public
lang: en
---


# Assessing DDoS Attack
## How to verify if the website is under DDoS attack

### Problem

A website is suspected to be under DDoS attack, and the client needs to address that.

* * *


### Solution

Make sure to check [Article #171: Website Down](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Web_Vulnerabilities/171-Website_down.html) before proceeding with below steps.  

#### Guide Questions

- What is the error the client is encountering?
- Is the website loading intermittently?
- How long is the website in this condition already? 
- Is it not loading at all?
- Is the web host accessible?
- Did they experience this issue before?
- Did the client receive a notification from their web hosting provider? What could be the problem?
- Can the client extract or request the traffic logs? 
- What is the bandwidth capacity of their website? What is the average traffic they receive everyday?
- Is the website already using DDoS protection services (e.g. Cloudflare, Deflect, etc.)?
- What could be the motivation of the attackers for taking down their website?

#### Analyzing Traffic Logs 

1. Request the traffic logs from the client. It should capture at least one-month log containing the date of attacks. The steps below are applicable for GoDaddy hosted websites:

   a. Log in to the account.
   
   b. Click Web Hosting.
   
   c. Next to the account, click Manage.
   
   d. From the Files &amp; FTP menu, select FTP File Manager.
   
   e. From the directory structure that displays, click Apache Logs.

2. Get useful information from the traffic logs:

   a. Top 5 IP addresses source:
   ```bash
   awk '{ print $1}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```
   
   b. Top 5 User-Agents used:
   ```bash
   awk -F\" '{print $6}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```
 
   c. Top 5 website directories being requested:
   ```bash
   awk -F\" '{print $2}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```
 
   d. Top 5 time with largest traffic per minute:
   ```bash
   awk '{ print $4}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```

   e. Top 5 HTTP Refferers:
   ```bash
   awk -F\" '{print $4}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```
 
   f. Top 5 HTTP codes responded by the server:
   ```bash
   awk '{ print $9}' traffic.log | sort | uniq -c | sort -nr | head -n 5
   ```

   **Note**: Top results could be changed into 2,3,4,6, etc. depending which could provide better understanding of the logs. This could be done by changing the numeric value after ``head -n``. Also, the above commands are applicable for ``Apache logs``

   g. Get the average traffic per day: 
   ```bash
   # Excluding the date of attack indicated by ``'DD/Mmm'`` (example: 15/May):
   awk '{print $4}' traffic.log | cut -d: -f1 | uniq -c | grep -v 'DD/Mmm' | awk '{s+=$1} END {print s/NR}'

   # Only the date dates of attack, the dates are indicated after -e flag
   awk '{print $4}' traffic.log | cut -d: -f1 | uniq -c | grep -e 'DD/Mmm' -e 'DD/Mmm'
   ```
   If the client is hosted with GoDaddy, their traffic statistics could be check through [here](https://godaddy.com/help/view-traffic-statistics-logs-and-metrics-for-my-linux-hosting-website-16179)

   h. Get the total traffic in kilobytes for the duration of the logs given:
   ```bash
   awk '{totalkb += $10} END {printf ("%9.2f Kb\n", totalkb/1024);}' traffic.log
   ```

3. Interpret the results from Step 2:

   a. Check the information about the IP addresses: owner, location, if blacklisted, etc.

   b. Check what are the User-Agent being used: legitimate visitors, crawlers, bots, etc.

   c. Check the directories being requested: specific website page, random characters, unavailable page, etc.

   d. Check if the largest traffic per minute's date is the date of the suspected attack, compare it with the average traffic they have in normal days.

   e. Check if the referrers are just coming from social media websites, the same with the IP sources, since the articles could be shared on the platforms. This could appear to be requested by one individual, but in reality it is coming from a link shared via social media platform and clicked by several followers.

   f. Check if the codes responded by the server are resource unavailability, unauthorized access, server error, etc.

   g. Check if the traffic per day during normal days and the dates of attack are different. 

   h. Check if the total traffic for the duration of logs is within the bandwidth of their hosting plan. It could cause a problem with the availability of the website once the traffic they are catering is larger with the limit of their bandwidth. Bandwidth limi for websites hosted with GoDaddy could be check through [here](https://godaddy.com/help/view-traffic-statistics-logs-and-metrics-for-my-linux-hosting-website-16179)

4. Proceed on helping the client set-up DDoS protection service if necessary:
   - [Deflect](Deflect.ca)
      - [Article #137: How to set up Deflect DDoSP for a client](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/DDoS/137-Set_Up_Deflect.html)
   - [CloudFlare](https://www.cloudflare.com/galileo)
      - [Article #72: Project Galileo Onboarding](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/DDoS/72-Project_Galileo_Onboarding.html)

5. If they already have DDoS protection installed, it is recommended to reach out to [Qurium](https://www.qurium.org/). If the client agreed to proceed with the referral to Qurium, send an email to tech@virtualroad.org containing:
   
   - Analysis of the traffic logs
   - Traffic logs: back-end server, and from DDoS protection

* * *

### Comments


#### Related Articles

- [Article #171: Website Down](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Web_Vulnerabilities/171-Website_down.html)
- [Article #137: How to set up Deflect DDoSP for a client](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/DDoS/137-Set_Up_Deflect.html)
- [Article #72: Project Galileo Onboarding](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/DDoS/72-Project_Galileo_Onboarding.html)
