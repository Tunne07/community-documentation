---
title: Google Account Security for an Individual
keywords: Google, account security, verification, authentication, 2FA, 2-factor authentication
last_updated: April 20, 2020
tags: [account_security, articles]
summary: "The client's Google account has suffered or been implicated in a security incident"
sidebar: mydoc_sidebar
permalink: 90-Google_2FA.html
folder: mydoc
conf: Public
lang: en
---


# Google Account Security for an Individual
## Advice on how to better secure a Google account

### Problem

- The client's Google account might have been hacked
- The client received a recovery link for their Google account without having asked for it
- The client has received phishing emails trying to obtain their Google password
- Emails have been sent from the client's Gmail account without the owner's permission
- Worries about account access attempts from other devices which are not owned by the client


* * *


### Solution

#### Reset account password

In case of a significant security incident, the client should change their account password immediately after confirming their contact and recover information is correct (in case the changed password is later forgotten). This can be done at [https://myaccount.google.com/security](https://myaccount.google.com/security) then clicking **Password**.

* * *

#### Google Account Security Checkup

There are a number of indicators and methods of compromise of a Google account, including:

- Unknown devices accessing the account
- Changes to sensitive security setting like recovery options, passwords, security settings
- Stolen passwords
- 3rd party apps given access to account data and functions
- Account sharing and forwarding settings in Gmail

Google provides a convenient click-through Security Check-up we should advise clients to use at [https://myaccount.google.com/security-checkup/](https://myaccount.google.com/security-checkup/).

* * *

#### Enable Two-factor authentication

Set up the account with two-factor authentication. To set up this service, the client can follow the steps described in [this page](https://www.google.com/landing/2step/).

We should warn the client about the risks of using SMS-based two-factor authentication and advise them to use an app or a security key.

- [Infographic on the various types of multi-factor authentication](https://www.accessnow.org/decoding-two-factor-authentication-solution-right/)
- [Using a security key for 2-step verification](https://support.google.com/accounts/answer/6103523?hl=en) - *Please note that security keys are not available in all countries. Access Now Helpline can try to assist the client in acquiring them if needed.*

When setting up two-factor authentication, we should advise the client to confirm their contact information is up to date and set up sufficient backup methods to obtain codes to avoid being locked out of their account.

If the client chooses to use a mobile app or a security key, it is not necessary to provide a phone number to set up two-factor authentication on Google.

* * *

#### Additional GMail settings to check

Some security-relevant settings should be checked under the Gmail Setting Section. Uses can access that by clicking the Gear icon -> Settings from their inbox or from [this link](https://mail.google.com/mail/u/0/#settings/general):

- Under the **Account and Import** tab, check for any address added under **Grant access to your account: (Allow others to read and send mail on your behalf)**
- Under the **Forwarding and IMAP** tab, check for any forwarding addresses configured under **Forwarding** in case _Forward a copy of incoming mail to_ is enabled
- Check the **Filters and blocked addresses** for any unknown or unwanted setting
- Check under the **Add-ons** tab for any unknown or unwanted add-ons

* * *

#### Advanced Protection Program

For certain high-risk situations, we can suggest the client to activate the [Advanced Protection Program](https://landing.google.com/advancedprotection/), which adds more measures to common 2-factor authentication:

- Access is only enabled through two-factor authentication based on a security key
- Third-party apps cannot access emails and Drive files
- Services can only be accessed through the Chrome browser

This adds a further layer of protection, but can also be limiting for the client: for example it won't be possible to use Thunderbird and Enigmail if their account is enrolled in the Advanced Protection Program, and if the client loses access to the account and their security keys, it will be more difficult to recover the account. So this program should only be recommended for particular situations, for example where there is an enhanced risk of spearphishing and the client does not encrypt their email.

* * *

### Comments

Google maintains an account [security check-list](https://support.google.com/mail/checklist/2986618?hl=en) which we should advise the client to use.

If the client is part of an organizational G-Suite domain, we should suggest their administrator follow [Article #381 for G-Suite security review and hardening](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/381-G-Suite-Domain-Security.html).

* * *

### Related Articles
