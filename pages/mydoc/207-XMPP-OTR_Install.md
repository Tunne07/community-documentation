---
title: Install XMPP+OTR on Linux, Windows and macOS
keywords: encrypted chat, secure IM, secure instant messaging, encryption, end-to-end encryption, XMPP, OTR, Linux, Windows, macOS, Mac, installation
last_updated: August 26, 2019
tags: [secure_communications, articles]
summary: "A client has identified XMPP+OTR as the best option for securing their chat communications and needs guidance for installing Pidgin and the OTR plugin or Adium in their computer."
sidebar: mydoc_sidebar
permalink: 207-XMPP-OTR_Install.html
folder: mydoc
conf: Public
ref: XMPP-OTR_Install
lang: en
---


# Install XMPP+OTR on Linux, Windows and macOS
## How to install Pidgin/Adium & OTR to enable encrypted instant messaging communication

### Problem

Often clients rely on insecure services like Skype and Facebook to communicate sensitive information over instant messaging. Using insecure channels to communicate may cause data leaks and put sensitive information in the wrong hands.

If the client decides to use [XMPP](https://en.wikipedia.org/wiki/XMPP) as a secure alternative, they might need guidance on how to install an XMPP client and set up [OTR](https://otr.cypherpunks.ca/) in their computer in order to make end-to-end encryption possible.


* * *


### Solution

We should first make sure that the client has considered all possible end-to-end encrypted instant messaging tools, and understands the [limitations of XMPP+OTR](141-Secure_Chat_Mobile.html#xmpp), including the fact that it only supports encryption for 1:1 communications.

- [Article #141: Secure Chat Tools for mobile devices](141-Secure_Chat_Mobile.html) includes many options, some of which are multi-platform and can be used both on computers and mobile phones. Please submit it to the client to help them choose the best solution for them.

If the client has decided they want to use XMPP and OTR for their encrypted chat communications, we may recommend the use of Adium on macOS, or Pidgin and the OTR plugin on Windows and Linux.

What follows are instructions for the installation and correct configuration of these tools on the different operating systems.


#### Installation

##### Windows: Pidgin + OTR plugin

1. Install Pidgin
    1. Download the installer [here](https://pidgin.im/download/windows) and double-click it to launch it.
    2. A prompt will appear asking for permission to make changes to the device - click "Yes".
    3. Select the preferred language.
    4. The setup wizard will suggest to close all other applications. Follow the advice, closing all open applications, and click "Next".
    5. Accept the License Agreement clicking "Next".
    6. In the "Choose Components", the default settings are fine, but the client might want to add the localization in their language. Click "Next" when done.
    7. In the "Choose Install Location" window, the default location is usually fine. Click "Install" to launch the installation.
    8. Click "Next" and then "Finish" to complete the installation.

2. Install OTR
    1. Download the installer [here](https://otr.cypherpunks.ca/index.php#downloads).
    2. An optional step, which is strongly recommended for high-risk users, is to verify the installer file against its GPG signature. If this is not your case skip to point 3.
        - To download the signature, right-click on "sig", in brackets next to the installer, and choose "Save link as" to download the file. Download the file in the same location where you have downloaded the installer. Both files will have the same name, with one ending in .exe and the other in .exe.asc
        - Install GPG4Win following [these instructions](https://guides.accessnow.org/PGP_Encrypted_Email_Windows.html#download-and-install-gpg4win) and launch Kleopatra.
        - Download the [OTR developers' GPG release signing key](https://otr.cypherpunks.ca/gpgkey.asc) and import it with Kleopatra by clicking on "File" and "Import" and selecting the file you've just downloaded.
        - in Kleopatra, right-click the key you've just imported and select "Change Certification Trust".
        - Select "I believe checks are very accurate" and click OK.
        - In the top panel of Kleopatra, click "Decrypt/Verify", then select the OTR installer.
        - Kleopatra will check that the file matches with its signature and has been created with the right GPG key. At the end you should see a green bar and a message that says "Verified 'pidgin-otr-x.x.x.exe' with 'pidgin-otr-x.x.x.exe.asc'. If so, you can be sure that the installer is not corrupted and you can proceed to the installation.
    3. Double-click the .exe file to launch the installer.
    4. A prompt will appear asking for permission to make changes to the device - click "Yes", then "Next".
    5. Accept the License Agreement clicking "I Agree".
    6. In the "Choose Install Location" window, the default location is usually fine. Click "Install" to launch the installation.
    7. Click "Finish" to complete the installation.


##### Linux: Pidgin + OTR plugin

Most Linux distributions come with Pidgin and the OTR plugin pre-installed. In case you need to install them, you should use your operating system's standard package management tool, either by command line, or through your package manager GUI.

- Debian Base:
    1. Command line:
    
            sudo add-apt-repository ppa:pidgin-developers/ppa
            
            sudo apt-get update
            
            sudo apt-get install pidgin pidgin-otr


    2. Via GUI - Package Manager

        - Ubuntu:
            1. Go to "System" > "Administration" > "Synaptic Package Manager".
            2. Type in "Pidgin" under quick search.
            3. To install Pidgin, scroll for "Pidgin" then right-click on it and select "Mark for installation".
            4. To install the OTR plugin, scroll for "Pidgin-otr" then right-click on it and select "Mark for installation".
            5. Click the Button "Apply".

            [Pidgin on Ubuntu Community Wiki](https://help.ubuntu.com/community/Pidgin#Installation_.26_Start_Up)

        - Linux Mint:
            1. Click "Menu" > and search for "Mint Software Center".
            2. Enter your password.
            3. Search for "Pidgin" in the upper right corner.
            4. Double-click "Pidgin" at the top of the list and click the button "Install".
            5. Search for "Pidgin-otr" in the upper right corner.
            6. Double-click "Pidgin-otr" at the top of the list and click the button "Install".

##### macOS: Adium

1. Download Adium from the [official website](https://adium.im/)
2. Double-click the .dmg file.
3. Drag/move the Adium icon to the applications folder to install the application.


* * *


### Comments

Proceed to [Article #208: Set-up Instructions for XMPP and OTR on Linux, Windows and macOS](208-XMPP-OTR_Setup.html) for setting up the client and enabling OTR.

Also see the EFF's Security Self-Defense Guides on setting up OTR, with instructions
for installation and set-up for Adium on macOS and Pidgin+OTR on Linux:

- [macOS](https://ssd.eff.org/en/module/how-use-otr-macos)
- [Linux](https://ssd.eff.org/en/module/how-use-otr-linux)

If the client needs to use XMPP and OTR in a mobile device, please see the [section on XMPP](141-Secure_Chat_Mobile.html#xmpp) in [Article #141: Secure Chat Tools for mobile devices](141-Secure_Chat_Mobile.html).


* * *


### Related Articles

- [Article #141: Secure Chat Tools for mobile devices](141-Secure_Chat_Mobile.html)
- [Article #208: Set-up Instructions for XMPP and OTR on Linux, Windows and macOS](208-XMPP-OTR_Setup.html)
