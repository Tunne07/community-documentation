---
title: Como Circunventar o Grande Firewall na China
keywords: circumvention, Great Firewall, China, VPN, Tor, censorship
last_updated: November 1, 2021
tags: [anonymity_circumvention, articles]
summary: "Client needs to overcome the Great Firewall in Mainland China"
sidebar: mydoc_sidebar
permalink: 110-VPN_China.html
folder: mydoc
conf: Public
lang: pt
---


# Como Circunventar o Grande Firewall na China
## Recomendações VPN para a China Continental

### Problema

Um cliente precisa contornar a Grande Firewall, seja porque vive, ou está viajando para a China Continental, mas:

- Os servidores VPN são freqüentemente bloqueados na China Continental.
- Há algumas evidências de que o Great Firewall tem a capacidade de bloquear seletivamente o tráfego VPN.
- Alguns provedores de VPN não respeitam a privacidade dos usuários.
- O Tor é bloqueado na China Continental.

* * *


#### Solução

#### VPNs

A solução que propomos ao cliente dependerá do que ele precisar.

Se o usuário estiver apenas interessado em contornar, ele pode utilizar uma VPN.

Algumas dessas ferramentas funcionarão em algumas regiões ou circunstâncias e não em outras, e algumas delas podem ser bloqueadas no nível do ISP. A melhor estratégia é criar várias VPNs antes que o cliente viaje para a China, de modo que eles tenham mais de uma opção no caso de uma não funcionar.

Um bom recurso para identificar os serviços VPN disponíveis e funcionais na China é [GreatFire](https://cc.greatfire.org/en).

Outro recurso é [GFW Report](https://gfw.report/) que analisa como o GFW identifica servidores VPN baseados em Shadowsocks e fornece orientações para evitar a detecção para [servidores](https://gfw.report/blog/ss_tutorial/en/) e [usuários](https://gfw.report/blog/ss_advise/en/).

#### Tor

O cliente deve instalar o Tor Browser antes de viajar, uma vez que o website do Projeto Tor está bloqueado na China. Eles também podem usar o serviço [GetTor](https://gettor.torproject.org/pt-BR/), que lhes enviará por e-mail os links para download.

Na China as conexões diretas com a rede Tor são bloqueadas. Para contornar o Grande Firewall, é necessário usar uma ponte Tor. Um guia de como usar as pontes Tor na China pode ser encontrado [aqui](https://support.torproject.org/censorship/connecting-from-china/).

Às vezes, as pontes embutidas no Tor Browser e o serviço de e-mail através de bridges@torproject.org não funcionam para os usuários na China. No entanto, as pontes privadas normalmente ainda funcionam. Instruções sobre como usar uma ponte privada podem ser encontradas [aqui](https://tb-manual.torproject.org/bridges/).

Se um cliente precisar de mais ajuda para obter um endereço de uma ponte privada, ele pode entrar em contato com o Projeto Tor enviando um e-mail para sua equipe de suporte para frontdesk@torproject.org.

  - [FAQ sobre como contornar a censura usando o Tor Browser](https://support.torproject.org/censorship/)
  - [Informações sobre transporte evasivo e conectável](https://tb-manual.torproject.org/circumvention/)
  - [Como adicionar pontes no Navegador Tor](https://tb-manual.torproject.org/bridges/)
  - Artigo sobre o que fazer quando [Tor é bloqueado em um país] (https://blog.torproject.org/blog/breaking-through-censorship-barriers-even-when-tor-blocked) (publicado em 3 de agosto de 2016)
  - [Outro guia do Projeto Tor sobre como estabelecer pontes](https://bridges.torproject.org/)
  - [Um guia mantido pela comunidade para o uso de pontes e o estabelecimento de um relé de ponte](https://sigvids.gitlab.io/)


#### Outras aplicações com transportes plugáveis


* * *


### Comentários


* * *

### Artigos relacionados

- [Artigo #175: FAQ - Circunvenção &amp; ferramentas de anonimato](175-Circumvention_Anonymity_tools_list.html)


Traduzido com a versão gratuita do tradutor - www.DeepL.com/Translator
