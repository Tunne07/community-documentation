---
title: Encrypt an External Storage Device Using Disk Utility on Mac
keywords: FDE, full-disk encryption, device security, external device, storage device
last_updated: August 19, 2019
tags: [devices_data_security, articles]
summary: "A client is using a Mac and their external device might be examined by an untrusted authority or risks being lost or stolen."
sidebar: mydoc_sidebar
permalink: 190-Encrypt_External_Storage_Device_Mac.html
folder: mydoc
conf: Public
lang: en
---


# Encrypt an External Storage Device Using Disk Utility on Mac
## How to encrypt a removable storage device with a Mac

### Problem

A client who uses a Mac needs to protect the data stored in an external device, as the device might be stolen, lost, or inspected by untrusted authorities.

This data could be used against the client, who risks physical or emotional abuse, arrest, detainment, etc., or could be used against others in a similar way.


* * *


### Solution

* Is the storage device already formatted for use by a Mac? If not, send [this link](https://support.apple.com/en-us/HT208496) to the client first.

* After the client has properly formatted their storage device, send [this link](https://support.apple.com/en-us/HT208496) to help them encrypt it.

* Remind the client to use a strong password, as the data they are trying to protect depends on it. Note that they must remember this password, as there will be no other way to recover the data once encrypted.

* Let the client know that encrypting drives takes time. If the disk they are encrypting is large, it may take hours - even overnight.


* * *


### Comments



* * *

### Related Articles

- [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)