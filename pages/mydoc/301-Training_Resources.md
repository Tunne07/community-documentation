---
title: Training Resources
keywords: trainings, digital security trainings, workshops, resources, ADIDS
last_updated: July 13, 2020
tags: [direct_intervention, articles]
summary: "An organization or group of activists has requested a training or workshop and we are looking for reference resources to prepare it."
sidebar: mydoc_sidebar
permalink: 301-Training_Resources.html
folder: mydoc
conf: Public
lang: en
---



# Training Resources
## A list of resources for digital security trainings

### Problem

An organization or group of activists has requested a training or workshop and we are looking for reference resources to prepare it.


* * *


### Solution

The recommended framework employed by the Digital Security Helpline to design digital security trainings and workshops is the Activity-Discussion-Inputs-Deepening-Synthesis – or ADIDS – model.

Please find further information about it in the following resources:

- [How to Approach Adult Learning](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/) – Level Up
- [How to Design Sessions Using ADIDS](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/) – Level Up
- [A training of trainers module on adult learning and ADIDS](https://www.fabriders.net/tot-adids/) – Fabriders

The Digital Security Helpline strives to adopt a holistic approach to thinking of, practicing and training on digital security. This approach integrates digital security, information security, as well as self-care, and well-being into traditional security management practices, and helps individuals to strengthen their ability to mitigate and reduce threats effectively and sustainably.

Please find further information about holistic security and how to apply this approach to digital security training in the following resources:

- [Holistic Security Manual](https://holistic-security.tacticaltech.org/index.html)
- [Holistic Security Trainers' Manual](https://holistic-security.tacticaltech.org/news/trainers-manual.html) including articles on best practices in facilitation of security and protection trainings for human rights defenders, and sample sessions designed following the ADIDS model.

Several trusted Helpline partners and collaborators across the digital security training community have developed educational resources that can be of great help to support civil society workers, human rights defenders, journalists and activists.

The following resources offer several examples of sessions, activities and exercises that can be very useful in the development of a digital security training or workshop:

- [Level Up](http://level-up.cc/) – A collection of resources for the global digital safety training community.
- [Security Education Companion](https://sec.eff.org) – Electronic Frontier Foundation. A resource for new and soon-to-be digital security trainers.
- [Digital Security Trainer’s Assistant](https://safesisters.net/wp-content/uploads/2019/09/Digital-Safety-Trainers-Assistant-smaller.pdf) – Natasha Msonza. Guidance and recommendations for new and experienced trainers.
- [Me and My Shadow Training Curriculum](https://myshadow.org/train) – Tactical Technology Collective.
- [Digital Security Training Resources for Security Trainers, Fall 2019 Edition](https://medium.com/cryptofriends/digital-security-training-resources-for-security-trainers-spring-2017-edition-e95d9e50065e) – Rachel Weidinger, Cooper Quintin, Martin Shelton, Matt Mitchell.
- [Gendersec Training Curricula](https://gendersec.tacticaltech.org/wiki/index.php/Gendersec_training_curricula) - Resource oriented towards the digital security needs of women human rights defenders and women's and LGBTQ rights activists.
- [FTX: Safety Reboot](https://ftxreboot.wiki.apc.org/index.php/Main_Page) – Association for Progressive Communications (APC) and collaborators. Training curriculum for trainers who work with women’s rights and sexual rights activists.
- [Digital First Aid Kit](https://digitalfirstaid.org/en/index.html) – RaReNet (Rapid Response Network) and CiviCERT. DFAK is a resource to help rapid responders, digital security trainers, and tech-savvy activists to better protect themselves and the communities they support against the most common types of digital emergencies, such as: devices or data loss, devices and accounts acting suspiciously, websites not working, online impersonation, and online harassment.
- [Digital Security Manual for Human Rights Defenders](https://equalit.ie//esecman/intro.html) - Front Line Defenders. Published in 2007, this manual still provides helpful guidance on how to discuss essential digital security concepts relevant to and grounded in the lived experienced of human rights defenders.

In case you want to complement a training effort with assigning online learning modules on key digital security concepts you can refer to:

- [Totem](https://totem-project.org), a platform providing short online lessons designed to help journalists and activists use digital security and privacy tools and tactics
- [Advocacy Assembly](https://advocacyassembly.org/en/)

If you are interested in learning more about the processes by which human rights defenders learn about and adopt digital security practices, and the approaches and methodologies that digital security trainers have found most effective to support them, please check out this research project:

- [Digital Security in Human Rights: A Research Project About Training and Learning](https://secresearch.tacticaltech.org/) – Becky Kazansky and Carol Waters, with Tactical Technology Collective

* * *


### Comments



* * *


### Related Articles
