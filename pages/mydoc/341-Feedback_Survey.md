---
title: Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Email to be sent to clients or requestors when closing a ticket"
sidebar: mydoc_sidebar
permalink: 341-Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: en
---


# Feedback Survey Template
## Email to be sent to clients or requestors when closing a ticket

### Body

Dear {{ beneficiary name }},

Thank you for contacting the Digital Security Helpline, run by the international human rights organization Access Now - https://accessnow.org.

This message is to notify you of the closure of your case titled "{{ email subject }}".

Your feedback is important to us. If you would like to provide feedback about your experience with Access Now Digital Security Helpline, please fill out the following survey:

https://form.accessnow.org/index.php/139723?lang=en&139723X20X841={{ ticket id }}

Your case number is:  {{ ticket id }}

If you have any further questions or concerns, please let us know and we will be happy to help.

Thank you,

{{ incident handler name }}
