---
title: PGP - add User ID - email template - Russian
keywords: email, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: August 12, 2021
tags: [secure_communications_templates, templates]
summary: "Письмо клиенту с инструкциями о том, как добавить новый ID пользователя к ключу PGP"
sidebar: mydoc_sidebar
permalink: 321-PGP_add_userid_email_ru.html
folder: mydoc
conf: Public
ref: PGP_add_userid_email
lang: ru
---


# PGP - add User ID - email template
## Email to client to explain how to add a user ID to their PGP key

### Body

Здравствуйте,

Чтобы добавить новый ID пользователя к вашему ключу PGP, требуется следующее: найти вашу пару ключей в менеджере ключей OpenPGP, добавить ваш новый ID к этой паре, опубликовать обновленный открытый ключ на серверах ключей.

Это можно сделать следующим образом:

1. В Thunderbird нажмите кнопку настроек и выберите "Enigmail" >; "Менеджер ключей".

2. В поле "Искать" укажите почтовый адрес, который вы связывали со своими ключами.

3. Щелкните правой кнопкой мыши по своему ключу PGP и выберите "Управление идентификаторами пользователя".

4. Нажмите кнопку "Добавить".

5. Введите новое имя и новый адрес e-mail, которые хотите связать со своим ключом PGP. Нажмите "ОК". В открывшемся окне введите пароль к вашему секретному ключу.

6. Щелкните правой кнопкой мыши по своему ключу PGP и выберите "Подгрузить открытые ключи на сервер ключей".

7. Ответьте на это наше письмо, сообщите свой новый ID. Пожалуйста, добавьте адрес help@accessnow.org в поле "Копия" и {{ ticket id }} в поле "Тема".

Будьте добры, напишите, если у вас есть вопросы или нужна какая-нибудь помощь.

С уважением,

{{ incident handler name }}



* * *


### Related Articles

- [Article #19: PGP - Add UserID](19-PGP_add_userID.html)
